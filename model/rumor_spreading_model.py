from collections import Counter, deque
from copy import deepcopy
from itertools import chain, compress
from math import log2, log, exp
from multiprocessing import Pool
from random import choice
from typing import Any, Dict, Optional, List, Union

import numpy as np
import networkx as nx
from networkx import Graph, DiGraph


class RumorSpreadingModel:
    """Class simulating model on a graph"""

    def __init__(self, graph: Union[Graph, DiGraph], memory_capacity: int):
        """
        :param graph: Graph on which we want to perform random walk
        """
        self._graph: DiGraph = graph if isinstance(graph, DiGraph) else graph.to_directed()
        nx.set_node_attributes(self._graph, {k: {'memory': deque(maxlen=memory_capacity)} for k in self._graph.nodes})
        self._zeros_per_node = {node: 0. for node in self._graph.nodes}

        self._number_of_nodes = len(self._zeros_per_node)

        self._statistics_vector = None
        self._acceptance_probabilities_matrix = None
        self._confidence_factor_beta = None

        self._name_ent = 'information_entropy'
        self._name_avg_ent = 'average_information_entropy'
        self._name_op_frag = 'opinion_fragmentation'
        self._name_rng_inf_spr = 'range_of_information_spread'

    @property
    def statistics_vector(self) -> Optional[Dict[str, List[float]]]:
        """
        :return: generated statistics vector. If trajectory wasn't generated returns None.
        """
        return self._statistics_vector

    @property
    def graph(self) -> DiGraph:
        """
        :return: graph as directed graph
        """
        return self._graph

    @property
    def acceptance_probabilities_matrix(self):
        if self._acceptance_probabilities_matrix is None and self._confidence_factor_beta is not None:
            self._acceptance_probabilities_matrix = {}
            degrees_dict = {node: log(degree) * self._confidence_factor_beta for node, degree in self._graph.degree()}
            for to_node in self._graph.nodes:
                max_degree_in_neighbourhood = max(degrees_dict[n] for n in self._graph.neighbors(to_node))
                for from_node in self._graph.neighbors(to_node):
                    self._acceptance_probabilities_matrix[(from_node, to_node)] = \
                        exp(degrees_dict[from_node] - max_degree_in_neighbourhood)
        return self._acceptance_probabilities_matrix

    def generate_trajectory(self, nodes_with_information: Dict[Any, str], steps: int,
                            conservation_factor_k: float, confidence_factor_beta: float):
        if conservation_factor_k < 0:
            raise ValueError("Conservation Factor must be non-negative")
        if any(node not in self._graph for node in nodes_with_information):
            raise IndexError("Starting node not in the graph")

        self._acceptance_probabilities_matrix = None
        self._confidence_factor_beta = confidence_factor_beta
        current_graph = deepcopy(self._graph)
        for node, information in nodes_with_information.items():
            current_graph.nodes[node]['memory'].appendleft(information)
        self._statistics_vector = {
            self._name_ent: [], self._name_avg_ent: [], self._name_op_frag: [], self._name_rng_inf_spr: []}

        for _ in range(steps):
            sent_messages = self._spreading_phase(current_graph, conservation_factor_k)
            current_graph = self._acceptance_and_updating_phase(current_graph, sent_messages)

        self._update_statistics(current_graph, *self.get_information_entropy(current_graph))

        return self._statistics_vector

    def _spreading_phase(self, current_graph, conservation_factor_k: float):
        information_entropy_per_node, most_salient_information_per_node = self.get_information_entropy(current_graph)

        self._update_statistics(current_graph, information_entropy_per_node, most_salient_information_per_node)

        max_information_entropy = max(information_entropy_per_node.values())
        random_vec = np.random.random(len(most_salient_information_per_node))
        sent_messages = {}

        if np.isclose(max_information_entropy, 0):
            distortion_factor = None
            probability_od_distortion = 1 / (exp(conservation_factor_k) + 1)
        else:
            distortion_factor = conservation_factor_k / max_information_entropy
            probability_od_distortion = None

        for (from_node, salient_info), rand in zip(most_salient_information_per_node.items(), random_vec):
            probability_od_distortion = \
                1 / (exp(conservation_factor_k - information_entropy_per_node[from_node] * distortion_factor) + 1) \
                    if distortion_factor is not None else probability_od_distortion
            if rand < probability_od_distortion:
                current_graph, salient_info = self._distort_information(current_graph, from_node, salient_info)
            for to_node in current_graph.neighbors(from_node):
                sent_messages[(from_node, to_node)] = salient_info
        return sent_messages

    def get_information_entropy(self, current_graph):
        information_entropy_per_node = self._zeros_per_node.copy()
        most_salient_information_per_node = {}
        for node, memory in filter(lambda x: len(x[1]), current_graph.nodes(data='memory')):
            memory_cnt = Counter(memory)
            most_salient_information_per_node[node] = memory_cnt.most_common(1)[0][0]
            information_entropy_per_node[node] = - sum(
                [f_i * log2(f_i) for f_i in (info_cnt / len(memory) for info_cnt in memory_cnt.values())])
        return information_entropy_per_node, most_salient_information_per_node

    def _update_statistics(self, current_graph, information_entropy_per_node, most_salient_information_per_node):
        self._statistics_vector[self._name_ent].append(list(information_entropy_per_node.values()))
        self._statistics_vector[self._name_avg_ent].append(
            self._average_from_list(list(information_entropy_per_node.values())))
        self._statistics_vector[self._name_op_frag].append(
            self.get_option_fragmentation(most_salient_information_per_node))
        self._statistics_vector[self._name_rng_inf_spr].append(self.get_range_of_information_spread(current_graph))

    @staticmethod
    def _distort_information(current_graph, node, salient_info):
        distorted_information = salient_info
        change_of_information_id = choice(range(len(distorted_information)))
        distorted_information = distorted_information[:change_of_information_id] + (
            '0' if distorted_information[change_of_information_id] == '1' else '1'
        ) + distorted_information[1 + change_of_information_id:]

        list_memory_of_node = list(current_graph.nodes[node]['memory'])[::-1]
        list_memory_of_node[list_memory_of_node.index(salient_info)] = distorted_information
        current_graph.nodes[node]['memory'].clear()
        current_graph.nodes[node]['memory'].extendleft(list_memory_of_node)
        return current_graph, distorted_information

    def _acceptance_and_updating_phase(self, current_graph, sent_messages):
        filtered_messages = self._accept_messages(sent_messages)
        for to_node, messages in filtered_messages.items():
            current_graph.nodes[to_node]['memory'].extendleft(messages)
        return current_graph

    def _accept_messages(self, sent_messages):
        random_vec = np.less(np.random.random(len(sent_messages)),
                             list(map(self.acceptance_probabilities_matrix.get, sent_messages)))
        filtered_messages = {}
        for (_, to_node), message in compress(sent_messages.items(), random_vec):
            filtered_messages[to_node] = filtered_messages.get(to_node, []) + [message]
        return filtered_messages

    def get_option_fragmentation(self, current_most_salient_information_per_node):
        list_of_most_salient_information = list(current_most_salient_information_per_node.values())
        list_of_most_salient_information_cnt = Counter(list_of_most_salient_information)
        return {info: cnt / self._number_of_nodes for info, cnt in list_of_most_salient_information_cnt.items()}

    def get_range_of_information_spread(self, current_graph):
        nodes_memories = list(chain.from_iterable(
            map(RumorSpreadingModel._get_unique_values_from_memory, current_graph.nodes(data='memory'))))
        nodes_memories_cnt = Counter(nodes_memories)
        return {info: cnt / self._number_of_nodes for info, cnt in nodes_memories_cnt.items()}

    @staticmethod
    def _average_from_list(l):
        return sum(l) / len(l)

    @staticmethod
    def _get_unique_values_from_memory(node_with_memory):
        used = set()
        return [x for x in node_with_memory[1] if x not in used and (used.add(x) or True)]


def generate_average_information_entropy(*_):
    model = RumorSpreadingModel(nx.barabasi_albert_graph(3000, 2), 320)
    model.generate_trajectory({choice(list(model.graph.nodes().keys())): '00000'}, 2000, 0., 30.)
    return model.statistics_vector['average_information_entropy']


if __name__ == '__main__':
    # print(generate_average_information_entropy())
    with Pool() as p:
        stat_args = p.map(generate_average_information_entropy, range(10))
    print(np.mean(stat_args, axis=0))
